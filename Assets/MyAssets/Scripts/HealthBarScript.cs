﻿using UnityEngine;
using System.Collections;

public class HealthBarScript : MonoBehaviour {
	public GameObject barPrefab;
	public GameObject owner;
	public int health = 10;
	
	private GameObject[] bars = new GameObject[10];

	// Use this for initialization
	void Start () {
		Respawn();
	}
	
	void Respawn()
	{
		health = 10;
		for (int i=0;i<10;i++)
		{
			Destroy(bars[i]);
			GameObject go = Instantiate(
				barPrefab,
				transform.position,
				gameObject.transform.rotation * Quaternion.Euler(0, i*36.0f, 0)
			) as GameObject;
			go.transform.localScale = transform.localScale;
			
			go.transform.parent = gameObject.transform;
			bars[i] = go;
		}
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(new Vector3(0,30.0f*Time.deltaTime,0));
	}
	
	void Damaged() {
		health--;
		if (health>=0)
		{
			float lerp = 0.1f * health;
			for (int i=0;i<health;i++)
			{
				Renderer rend = bars[i].GetComponentInChildren<Renderer>();
				if (lerp<=0.5f)
				{
					rend.material.SetColor("_Color", Color.Lerp(Color.red, Color.yellow, lerp*2.0f));
				}
				else
				{
					rend.material.SetColor("_Color", Color.Lerp(Color.yellow, Color.green, (lerp-0.5f)*2.0f));
				}

			}
			Destroy(bars[health]);
			
			if (health==0)
			{
				owner.SendMessage("Deth");
			}
			else
			{
				owner.SendMessage("Wound");
			}
		}
	}
}
