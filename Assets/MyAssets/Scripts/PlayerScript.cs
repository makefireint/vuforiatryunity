﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {
	public GameObject enemy;
	public GameObject healthBar;
	public GameObject blood;
	public GameObject bloodPool;
	
	public AudioClip[] atackSounds;
	public AudioClip dethSound;
	
	[HideInInspector]
	public bool dead = false;
	
	private Animator anim;
	private float radius;
	private ParticleSystem bloodSystem;

	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponent<Animator>();
		radius = enemy.GetComponent<SphereCollider>().radius*
					enemy.transform.localScale.x*
					enemy.transform.parent.localScale.x;
		healthBar.GetComponent<HealthBarScript>().owner = gameObject;
		bloodSystem = blood.GetComponent<ParticleSystem>();
	}
	
	void Respawn()
	{
		healthBar.SendMessage("Respawn");
		anim.Play("idle",-1,0);
		dead = false;
		bloodPool.SetActive(false);
	}	
	
	// Update is called once per frame
	void Update () {		
			AnimatorStateInfo state = anim.GetCurrentAnimatorStateInfo(0);
			if (state.IsName("attack"))
			{
				Vector3 distance = enemy.transform.parent.position - transform.parent.position;
				Quaternion targetRotation = Quaternion.LookRotation(distance.normalized, enemy.transform.parent.up);
				Vector3 targetPosition = enemy.transform.parent.position-distance.normalized*radius;

				if (state.normalizedTime<0.5f)
				{
					transform.position = 
						Vector3.Lerp(
							transform.parent.position,
							targetPosition,
							state.normalizedTime*2.0f);
												
					transform.rotation =
						Quaternion.Lerp(
							transform.parent.rotation,
							targetRotation,
							state.normalizedTime*2.0f);
				}
				else
				{
					transform.position = 
						Vector3.Lerp(
							transform.parent.position,
							targetPosition,
							(1.0f-state.normalizedTime)*2.0f);
					transform.rotation =
						Quaternion.Lerp(
							transform.parent.rotation,
							targetRotation,
							(1.0f-state.normalizedTime)*2.0f);

				}

			}
	}
	
	void Attack () {
		if (!dead)
		{
			AnimatorStateInfo state = anim.GetCurrentAnimatorStateInfo(0);
			if (state.IsName("idle"))
			{
				anim.Play("attack",-1,0);
			
				int r = Random.Range(0,atackSounds.Length);
				GetComponent<AudioSource>().PlayOneShot(atackSounds[r]);
			
				enemy.SendMessage("Damaged");
			}			
		}
		else
		{
			Respawn();
		}
	}
	
	IEnumerator Damaged () {
		yield return new WaitForSeconds(0.7f);
		healthBar.SendMessage("Damaged");
	}
	
	void Deth() {
		anim.Play("deth",-1,0);
		GetComponent<AudioSource>().PlayOneShot(dethSound);
		dead = true;
		bloodPool.SetActive(true);
	}
	void Wound() {
		anim.Play("wound",-1,0);
		ParticleSystem.EmissionModule psemit = bloodSystem.emission;
		psemit.enabled = false;
		bloodSystem.Stop ();
		psemit.enabled = true;
		bloodSystem.Emit(15);
	}
	
}
