﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GodScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		bool aTouch;
		Vector3 touchPos;

		if (Application.platform == RuntimePlatform.OSXEditor ||
			Application.platform == RuntimePlatform.OSXPlayer )
		{
		  aTouch = Input.GetMouseButtonDown(0);
		  touchPos = Input.mousePosition;
		} else {
		  aTouch = ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began));
		  touchPos = Input.touches[0].position;
		}



		if(aTouch)
		{
			Ray ray = Camera.main.ScreenPointToRay(touchPos);
			RaycastHit hit = new RaycastHit();
			if(Physics.Raycast (ray, out hit))
			{
				GameObject go = hit.transform.gameObject;
				go.SendMessage("Attack");
			}
		}
	}
	
	void OnGUI () {
		if (Screen.dpi>200)
		{
			GUI.matrix = Matrix4x4.Scale(new Vector3(2.0f,2.0f,2.0f));
		}
		if (GUI.Button (new Rect (20,20,80,40), "one marker")) {
				SceneManager.LoadScene("one_marker");
		}
		if (GUI.Button (new Rect (120,20,80,40), "two markers")) {
				SceneManager.LoadScene("two_markers");
		}
    }

}
